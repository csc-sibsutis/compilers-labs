#include <clang-c/Index.h>
#include <iostream>
#include <llvm/Support/CommandLine.h>

using namespace llvm;

static cl::opt<std::string> FileName(cl::Positional, cl::desc("Input file"),
                                     cl::Required);

int main(int argc, char **argv) {
  cl::ParseCommandLineOptions(argc, argv, "My tokenizer\n");

  CXIndex Index = clang_createIndex(0, 0);

  const char *Args[] = {"-I/usr/include", "-I."};

  CXTranslationUnit TranslationUnit = clang_parseTranslationUnit(
      Index, FileName.c_str(), Args, 2, NULL, 0, CXTranslationUnit_None);

  CXFile file = clang_getFile(TranslationUnit, FileName.c_str());
  CXSourceLocation LocStart =
      clang_getLocationForOffset(TranslationUnit, file, 0);
  CXSourceLocation LocEnd =
      clang_getLocationForOffset(TranslationUnit, file, 60);
  CXSourceRange Range = clang_getRange(LocStart, LocEnd);

  unsigned NumTokens = 0;
  CXToken *Tokens = NULL;

  clang_tokenize(TranslationUnit, Range, &Tokens, &NumTokens);
  for (unsigned i = 0; i < NumTokens; ++i) {
    enum CXTokenKind kind = clang_getTokenKind(Tokens[i]);
    CXString name = clang_getTokenSpelling(TranslationUnit, Tokens[i]);
    switch (kind) {
    case CXToken_Punctuation:
      std::cout << "PUNCTUATION(" << clang_getCString(name) << ") ";
      break;
    case CXToken_Keyword:
      std::cout << "KEYWORD(" << clang_getCString(name) << ") ";
      break;
    case CXToken_Identifier:
      std::cout << "IDENTIFIER(" << clang_getCString(name) << ") ";
      break;
    case CXToken_Literal:
      std::cout << "COMMENT(" << clang_getCString(name) << ") ";
      break;
    default:
      std::cout << "UNKNOWN(" << clang_getCString(name) << ") ";
      break;
    }
    clang_disposeString(name);
  }

  std::cout << std::endl;
  clang_disposeTokens(TranslationUnit, Tokens, NumTokens);
  clang_disposeTranslationUnit(TranslationUnit);
  return 0;
}
