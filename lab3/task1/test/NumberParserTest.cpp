#include <NumberParser.h>

#include <string>

#include <gtest/gtest.h>

class NumberParserTest : public ::testing::Test {
protected:
  void feedString(NumberParser &Parser, const std::string &S) const {
    for (const auto Ch : S) {
      Parser.feedChar(Ch);
    }
    Parser.feedChar(EOF);
  }

  bool isNumber(const std::string &S) const {
    NumberParser Parser;
    feedString(Parser, S);
    return Parser.isGood();
  }
};

TEST_F(NumberParserTest, EmptyString) {
  EXPECT_FALSE(isNumber(""));
}

TEST_F(NumberParserTest, Integer) {
  EXPECT_TRUE(isNumber("0"));
  EXPECT_TRUE(isNumber("1"));
  EXPECT_TRUE(isNumber("42"));
  EXPECT_TRUE(isNumber("1234567890"));
}

TEST_F(NumberParserTest, Float) {
  EXPECT_TRUE(isNumber("0.0"));
  EXPECT_TRUE(isNumber("1.0"));
  EXPECT_TRUE(isNumber("0.1"));
  EXPECT_TRUE(isNumber("0.01"));
  EXPECT_TRUE(isNumber("1234567890.1234567890"));
}

TEST_F(NumberParserTest, Exponent) {
  EXPECT_TRUE(isNumber("1E2"));
  EXPECT_TRUE(isNumber("1e2"));
  EXPECT_TRUE(isNumber("1e0"));
  EXPECT_TRUE(isNumber("0e1"));
  EXPECT_TRUE(isNumber("0e0"));
  EXPECT_TRUE(isNumber("0.0e0"));
  EXPECT_TRUE(isNumber("0.0e+0"));
  EXPECT_TRUE(isNumber("0.0e-0"));
  EXPECT_TRUE(isNumber("1234567890e1234567890"));
  EXPECT_TRUE(isNumber("42E+1"));
  EXPECT_TRUE(isNumber("42E-1"));
  EXPECT_TRUE(isNumber("42.0E+42"));
  EXPECT_TRUE(isNumber("42.0E-42"));
}

TEST_F(NumberParserTest, InvalidNumber) {
  EXPECT_FALSE(isNumber("foo"));
  EXPECT_FALSE(isNumber("1bar"));

  EXPECT_FALSE(isNumber("01"));
  EXPECT_FALSE(isNumber("1."));
  EXPECT_FALSE(isNumber(".1"));
  EXPECT_FALSE(isNumber("00.1"));

  EXPECT_FALSE(isNumber("+1"));
  EXPECT_FALSE(isNumber("-1"));

  EXPECT_FALSE(isNumber("."));
  EXPECT_FALSE(isNumber("E"));
  EXPECT_FALSE(isNumber("e"));

  EXPECT_FALSE(isNumber("1e1e1"));
  EXPECT_FALSE(isNumber("1.1.1"));
  EXPECT_FALSE(isNumber("1e"));
  EXPECT_FALSE(isNumber("1.e"));
  EXPECT_FALSE(isNumber("1.1e"));
  EXPECT_FALSE(isNumber("1.1e+"));
  EXPECT_FALSE(isNumber("1.1e-"));

  EXPECT_FALSE(isNumber("1+1"));
  EXPECT_FALSE(isNumber("1-1"));
  EXPECT_FALSE(isNumber("1+"));
  EXPECT_FALSE(isNumber("1-"));
}
