#pragma once

class NumberParser {
public:
  void feedChar(int Ch);
  bool isGood() const;

private:
  enum State {
    IntegralFirstDigit,
    ZeroBody,
    IntegralBody,
    FractionFirstDigit,
    FractionBody,
    Exponent,
    ExponentFirstDigit,
    ExponentBody,
    End,
    Error
  };

  State CurrentState = IntegralFirstDigit;
};
