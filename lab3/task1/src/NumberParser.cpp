#include "NumberParser.h"

#include <cctype>
#include <cstdio>

namespace {
enum CharClass { Digit, Zero, Dot, Exponent, Sign, Eof, Other };
} // namespace

static CharClass getCharClass(int Ch) {
  if (Ch == '0') {
    return Zero;
  }
  if (isdigit(Ch)) {
    return Digit;
  }
  if (Ch == '.') {
    return Dot;
  }
  if (tolower(Ch) == 'e') {
    return Exponent;
  }
  if (Ch == '+' || Ch == '-') {
    return Sign;
  }
  if (Ch == EOF) {
    return Eof;
  }
  return Other;
}

void NumberParser::feedChar(int Ch) {
  // clang-format off
  const State nextState[10][7] = {
    //                               Digit          Zero                 Dot  Exponent                Sign    Eof  Other
    /* IntegralFirstDigit */ {IntegralBody,     ZeroBody,              Error,    Error,              Error, Error, Error},
    /* ZeroBody           */ {       Error,        Error, FractionFirstDigit, Exponent,              Error,   End, Error},
    /* IntegralBody       */ {IntegralBody, IntegralBody, FractionFirstDigit, Exponent,              Error,   End, Error},
    /* FractionFirstDigit */ {FractionBody, FractionBody,              Error,    Error,              Error, Error, Error},
    /* FractionBody       */ {FractionBody, FractionBody,              Error, Exponent,              Error,   End, Error},
    /* Exponent           */ {ExponentBody, ExponentBody,              Error,    Error, ExponentFirstDigit, Error, Error},
    /* ExponentFirstDigit */ {ExponentBody, ExponentBody,              Error,    Error,              Error, Error, Error},
    /* ExponentBody       */ {ExponentBody, ExponentBody,              Error,    Error,              Error,   End, Error},
    /* End                */ {Error,               Error,              Error,    Error,              Error, Error, Error},
    /* Error              */ {Error,               Error,              Error,    Error,              Error, Error, Error}
  };
  // clang-format on
  const CharClass CurrentCharClass = getCharClass(Ch);
  CurrentState = nextState[CurrentState][CurrentCharClass];
}

bool NumberParser::isGood() const {
  return CurrentState != Error;
}
