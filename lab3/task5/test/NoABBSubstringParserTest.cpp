﻿#include <NoABBSubstringParser.h>

#include <string>

#include <gtest/gtest.h>

class NoABBSubstringParserTest : public ::testing::Test {
protected:
  void feedString(NoABBSubstringParser &Parser, const std::string &S) const {
    for (const auto Ch : S) {
      Parser.feedChar(Ch);
    }
    Parser.feedChar(EOF);
  }

  bool isValidString(const std::string &S) const {
    NoABBSubstringParser Parser;
    feedString(Parser, S);
    return Parser.isGood();
  }
};

TEST_F(NoABBSubstringParserTest, EmptyString) {
  EXPECT_FALSE(isValidString(""));
}

TEST_F(NoABBSubstringParserTest, ValidStrings) {
  EXPECT_TRUE(isValidString("a"));
  EXPECT_TRUE(isValidString("ab"));
  EXPECT_TRUE(isValidString("aab"));
  EXPECT_TRUE(isValidString("b"));
  EXPECT_TRUE(isValidString("bb"));
  EXPECT_TRUE(isValidString("bba"));
  EXPECT_TRUE(isValidString("abab"));
}

TEST_F(NoABBSubstringParserTest, InvalidStrings) {
  EXPECT_FALSE(isValidString("abb"));
  EXPECT_FALSE(isValidString("aabb"));
  EXPECT_FALSE(isValidString("abbb"));
  EXPECT_FALSE(isValidString("aabbb"));
  EXPECT_FALSE(isValidString("az"));
  EXPECT_FALSE(isValidString("AB"));
}
