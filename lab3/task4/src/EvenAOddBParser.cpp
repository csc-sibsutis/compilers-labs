﻿#include "EvenAOddBParser.h"

#include <cstdio>

namespace {
enum CharClass { A, B, Eof, Other };
} // namespace

static CharClass getCharClass(int Ch) {
  if (Ch == 'a')
    return A;
  if (Ch == 'b')
    return B;
  if (Ch == EOF)
    return Eof;
  return Other;
}

void EvenAOddBParser::feedChar(int Ch) {
  // clang-format off
  State nextState[11][4] = {
    //                         A           B    EOF  Other
    /* Empty      */ {DisAEmptyB,  EmptyAOkB,   End, Error},
    /* EmptyADisB */ {  DisADisB,  EmptyAOkB, Error, Error},
    /* EmptyAOkB  */ {   DisAOkB, EmptyADisB,   End, Error},
    /* DisAEmptyB */ { OkAEmptyB,    DisAOkB, Error, Error},
    /* DisADisB   */ {   OkADisB,    DisAOkB, Error, Error},
    /* DisAOkB    */ {    OkAOkB,   DisADisB, Error, Error},
    /* OkAEmptyB  */ {DisAEmptyB,     OkAOkB,   End, Error},
    /* OkADisB    */ {  DisADisB,     OkAOkB, Error, Error},
    /* OkAOkB     */ {   DisAOkB,    OkADisB,   End, Error},
    /* End        */ {     Error,      Error, Error, Error},
    /* Error      */ {     Error,      Error, Error, Error}
  };
  // clang-format on

  const CharClass CurrentCharClass = getCharClass(Ch);
  CurrentState = nextState[CurrentState][CurrentCharClass];
}

bool EvenAOddBParser::isGood() const {
  return CurrentState != Error;
}
