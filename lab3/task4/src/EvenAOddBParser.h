﻿#pragma once

class EvenAOddBParser {
public:
  void feedChar(int Ch);
  bool isGood() const;

private:
  enum State {
    Empty,
    EmptyADisB,
    EmptyAOkB,
    DisAEmptyB,
    DisADisB,
    DisAOkB,
    OkAEmptyB,
    OkADisB,
    OkAOkB,
    End,
    Error
  };

  State CurrentState = Empty;
};
