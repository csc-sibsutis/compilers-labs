﻿#include <EvenAOddBParser.h>

#include <string>

#include <gtest/gtest.h>

class EvenAOddBParserTest : public ::testing::Test {
protected:
  void feedString(EvenAOddBParser &Parser, const std::string &S) const {
    for (const auto Ch : S) {
      Parser.feedChar(Ch);
    }
    Parser.feedChar(EOF);
  }

  bool isValidString(const std::string &S) const {
    EvenAOddBParser Parser;
    feedString(Parser, S);
    return Parser.isGood();
  }
};

TEST_F(EvenAOddBParserTest, EmptyString) {
  EXPECT_TRUE(isValidString(""));
}

TEST_F(EvenAOddBParserTest, Balanced) {
  EXPECT_TRUE(isValidString("aa"));
  EXPECT_TRUE(isValidString("b"));
  EXPECT_TRUE(isValidString("aab"));
  EXPECT_TRUE(isValidString("aba"));
  EXPECT_TRUE(isValidString("baa"));
  EXPECT_TRUE(isValidString("abababa"));
}

TEST_F(EvenAOddBParserTest, Imbalanced) {
  EXPECT_FALSE(isValidString("a"));
  EXPECT_FALSE(isValidString("bb"));
  EXPECT_FALSE(isValidString("bba"));
  EXPECT_FALSE(isValidString("bab"));
  EXPECT_FALSE(isValidString("abb"));
  EXPECT_FALSE(isValidString("bababab"));
}

TEST_F(EvenAOddBParserTest, Invalid) {
  EXPECT_FALSE(isValidString("A"));
  EXPECT_FALSE(isValidString("BB"));
  EXPECT_FALSE(isValidString("aB"));
}
