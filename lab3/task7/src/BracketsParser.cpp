#include "BracketsParser.h"

#include "Lexer.h"

BracketsParser::BracketsParser(Lexer &lexer) : lexer_{lexer} {
}

bool BracketsParser::parse() {
  return parse_s() && lexer_.consume_token(-1);
}

bool BracketsParser::parse_s() {
  if (lexer_.peek_next_token() == '(') {
    return lexer_.consume_token('(') && parse_s() &&
           lexer_.consume_token(')') && parse_s();
  }

  return true;
}
