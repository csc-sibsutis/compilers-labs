#pragma once

#include <string>

class Lexer;

// Оригинальная грамматика:
//
// <s> ::= 0<s>1 | 01
//
// Грамматика после левой факторизации:
//
// <s> ::= 0<t>  (1)
// <t> ::= <s>1  (2)
//       | 1     (3)
//
// Таблица синтаксического анализа:
//
//   | 0 | 1 |
// s | 1 |   |
// t | 2 | 3 |
//
class ZerosOnesParser {
public:
  ZerosOnesParser(Lexer &lexer);

  bool parse();

private:
  bool parse_s();
  bool parse_t();

  Lexer &lexer_;
};
