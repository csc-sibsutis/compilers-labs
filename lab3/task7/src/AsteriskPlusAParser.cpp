#include "AsteriskPlusAParser.h"

#include "Lexer.h"

AsteriskPlusAParser::AsteriskPlusAParser(Lexer &lexer) : lexer_{lexer} {
}

bool AsteriskPlusAParser::parse() {
  if (lexer_.peek_next_token() == '+') {
    return lexer_.consume_token('+') && parse() && parse();
  }

  if (lexer_.peek_next_token() == '*') {
    return lexer_.consume_token('*') && parse() && parse();
  }

  if (lexer_.peek_next_token() == 'a') {
    return lexer_.consume_token('a');
  }

  return false;
}
