#pragma once

#include <string>

class Lexer;

// Оригинальная грамматика:
//
// <s> ::= +<s><s>  (1)
//       | *<s><s>  (2)
//       | a        (3)
//
// Таблица разбора:
//
//     | + | * | a
// <s> | 1 | 2 | 3
//
class AsteriskPlusAParser {
public:
  AsteriskPlusAParser(Lexer &lexer);
  bool parse();

private:
  Lexer &lexer_;
};
