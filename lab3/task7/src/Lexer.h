#pragma once

#include <fstream>

class Lexer {
public:
  explicit Lexer(std::istream &stream);

  int peek_next_token();
  bool consume_token(int t);

private:
  std::istream &stream_;
};
