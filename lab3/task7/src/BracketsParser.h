#pragma once

#include <string>

class Lexer;

// Оригинальная грамматика:
//
// <s> ::= <s>(<s>)<s> | ε
//
// Грамматика не принадлежит к классу LL(1) из-на наличия левой рекурсии.
//
// Устранение левой рекурсии в данном случае тривиально:
//
// <s> ::= (<s>)<s>  (1)
//       | ε         (2)
//
// Таблица синтаксического анализа:
//
//     | ( | $
// <s> | 1 | 2
//
class BracketsParser {
public:
  explicit BracketsParser(Lexer &lexer);
  bool parse();

private:
  bool parse_s();

  Lexer &lexer_;
};
