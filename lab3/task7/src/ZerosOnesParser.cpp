#include "ZerosOnesParser.h"

#include "Lexer.h"

ZerosOnesParser::ZerosOnesParser(Lexer &lexer) : lexer_{lexer} {
}

bool ZerosOnesParser::parse() {
  return parse_s();
}

bool ZerosOnesParser::parse_s() {
  return lexer_.consume_token('0') && parse_t();
}

bool ZerosOnesParser::parse_t() {
  if (lexer_.peek_next_token() == '1') {
    return lexer_.consume_token('1');
  }
  return parse_s() && lexer_.consume_token('1');
}
