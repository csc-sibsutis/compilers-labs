#pragma once

#include <sstream>
#include <string>

#include "Lexer.h"

template <class T> bool parse(const std::string &s) {
  std::stringstream ss{s};
  Lexer lexer{ss};
  T parser{lexer};
  return parser.parse() && lexer.consume_token(-1);
}
