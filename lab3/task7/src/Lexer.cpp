#include "Lexer.h"

Lexer::Lexer(std::istream &stream) : stream_{stream} {
}

int Lexer::peek_next_token() {
  if (stream_.eof()) {
    return -1;
  }
  return stream_.peek();
}

bool Lexer::consume_token(int t) {
  if (peek_next_token() != t) {
    return false;
  }
  if (!stream_.eof()) {
    stream_.get();
  }
  return true;
}
