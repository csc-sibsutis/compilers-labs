﻿#include <ZerosOnesParser.h>

#include <string>

#include <gtest/gtest.h>

#include <Parse.h>

static bool parse(const std::string &s) {
  return parse<ZerosOnesParser>(s);
}

TEST(ZerosOnesParserTest, EmptyString) {
  EXPECT_FALSE(parse(""));
}

TEST(ZerosOnesParserTest, Valid) {
  EXPECT_TRUE(parse("01"));
  EXPECT_TRUE(parse("0011"));
  EXPECT_TRUE(parse("000111"));
}

TEST(ZerosOnesParserTest, Invalid) {
  EXPECT_FALSE(parse("10"));
  EXPECT_FALSE(parse("0"));
  EXPECT_FALSE(parse("1"));
  EXPECT_FALSE(parse("001"));
  EXPECT_FALSE(parse("011"));
  EXPECT_FALSE(parse("00"));
  EXPECT_FALSE(parse("11"));
}
