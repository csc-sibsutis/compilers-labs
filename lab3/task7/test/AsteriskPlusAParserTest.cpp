#include <AsteriskPlusAParser.h>
#include <Parse.h>

#include <string>

#include <gtest/gtest.h>

static bool parse(const std::string &s) {
  return parse<AsteriskPlusAParser>(s);
}

TEST(AsteriskPlusAParserTest, EmptyString) {
  EXPECT_FALSE(parse(""));
}

TEST(AsteriskPlusAParserTest, Valid) {
  EXPECT_TRUE(parse("a"));
  EXPECT_TRUE(parse("+aa"));
  EXPECT_TRUE(parse("*aa"));
  EXPECT_TRUE(parse("++aa*aa"));
  EXPECT_TRUE(parse("++*aaa*a+aa"));
}

TEST(AsteriskPlusAParserTest, Invalid) {
  EXPECT_FALSE(parse("-"));
  EXPECT_FALSE(parse("+"));
  EXPECT_FALSE(parse("*"));
  EXPECT_FALSE(parse("+*"));
  EXPECT_FALSE(parse("*+"));
  EXPECT_FALSE(parse("aa"));
  EXPECT_FALSE(parse("a+"));
  EXPECT_FALSE(parse("a*"));
}
