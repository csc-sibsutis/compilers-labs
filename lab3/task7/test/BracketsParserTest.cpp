#include <BracketsParser.h>

#include <string>

#include <gtest/gtest.h>

#include <Parse.h>

static bool parse(const std::string &s) {
  return parse<BracketsParser>(s);
}

TEST(BracketsParserTest, EmptyString) {
  EXPECT_TRUE(parse(""));
}

TEST(BracketsParserTest, Valid) {
  EXPECT_TRUE(parse("()"));
  EXPECT_TRUE(parse("(())"));
  EXPECT_TRUE(parse("((()))"));
  EXPECT_TRUE(parse("()()()"));
  EXPECT_TRUE(parse("()(())((()))"));
  EXPECT_TRUE(parse("(()())"));
}

TEST(BracketsParserTest, Invalid) {
  EXPECT_FALSE(parse("(a)"));
  EXPECT_FALSE(parse(")("));
  EXPECT_FALSE(parse("())"));
  EXPECT_FALSE(parse("("));
  EXPECT_FALSE(parse("(("));
}
