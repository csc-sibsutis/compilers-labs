#include <OrderedVowelsParserImperative.h>
#include <OrderedVowelsParserSwitch.h>
#include <OrderedVowelsParserTable.h>

#include <gtest/gtest.h>

template <class T>
std::unique_ptr<OrderedVowelsParser> CreateOrderedVowelsParser();

template <>
std::unique_ptr<OrderedVowelsParser>
CreateOrderedVowelsParser<OrderedVowelsParserImperative>() {
  return std::make_unique<OrderedVowelsParserImperative>();
}

template <>
std::unique_ptr<OrderedVowelsParser>
CreateOrderedVowelsParser<OrderedVowelsParserSwitch>() {
  return std::make_unique<OrderedVowelsParserSwitch>();
}

template <>
std::unique_ptr<OrderedVowelsParser>
CreateOrderedVowelsParser<OrderedVowelsParserTable>() {
  return std::make_unique<OrderedVowelsParserTable>();
}

template <class T> class OrderedVowelsParserTest : public ::testing::Test {
protected:
  OrderedVowelsParserTest() : Parser{CreateOrderedVowelsParser<T>()} {}

  void feedString(const std::string &S) {
    for (const auto Ch : S) {
      Parser->feed(Ch);
    }
    Parser->feed(EOF);
  }

  bool isValidString(const std::string &S) {
    feedString(S);
    return Parser->isGood();
  }

  std::unique_ptr<OrderedVowelsParser> Parser;
};

using ::testing::Types;

using Implementations =
    Types<OrderedVowelsParserImperative, OrderedVowelsParserSwitch,
          OrderedVowelsParserTable>;

TYPED_TEST_CASE(OrderedVowelsParserTest, Implementations);

TYPED_TEST(OrderedVowelsParserTest, EmptyString) {
  ASSERT_FALSE(this->isValidString(""));
}

TYPED_TEST(OrderedVowelsParserTest, OnlyVowels) {
  ASSERT_TRUE(this->isValidString("aeiou"));
}

TYPED_TEST(OrderedVowelsParserTest, VowelsWithCons) {
  ASSERT_TRUE(this->isValidString("azeqiworut"));
}

TYPED_TEST(OrderedVowelsParserTest, VowelsUnordered) {
  ASSERT_FALSE(this->isValidString("uoiea"));
}

TYPED_TEST(OrderedVowelsParserTest, VowelsMissing) {
  ASSERT_FALSE(this->isValidString("a"));
}

TYPED_TEST(OrderedVowelsParserTest, InvalidChar) {
  ASSERT_FALSE(this->isValidString("1"));
}

TYPED_TEST(OrderedVowelsParserTest, InvalidCharEnd) {
  ASSERT_FALSE(this->isValidString("aeiou1"));
}

TYPED_TEST(OrderedVowelsParserTest, UppercaseVowels) {
  ASSERT_FALSE(this->isValidString("AEIOU"));
}
