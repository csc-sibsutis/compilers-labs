#include "OrderedVowelsParserImperative.h"

#include <array>
#include <cctype>

// Попытка взять первый вариант и избавиться от копипасты.
// Плюс: стало компактнее.
// Минус: не наглядно, автомат неочевиден.
void OrderedVowelsParserImperative::feed(int Ch) {
  if (CurrentState == End) {
    CurrentState = Error;
    return;
  }

  if (CurrentState == Error) {
    return;
  }

  if (CurrentState != ExpectEOF && Ch == EOF) {
    CurrentState = Error;
    return;
  }

  if (!isalpha(Ch) && !islower(Ch) && Ch != EOF) {
    CurrentState = Error;
    return;
  }

  const std::array<std::pair<char, State>, 6> Expected = {{{'a', ExpectE},
                                                           {'e', ExpectI},
                                                           {'i', ExpectO},
                                                           {'o', ExpectU},
                                                           {'u', ExpectEOF},
                                                           {EOF, End}}};

  if (Expected[CurrentState].first == Ch) {
    CurrentState = Expected[CurrentState].second;
  }
}
