#pragma once

#include "OrderedVowelsParser.h"

class OrderedVowelsParserSwitch : public OrderedVowelsParser {
public: // OrderedVowelsParser
  void feed(int Ch) override;
};
