#pragma once

#include "OrderedVowelsParser.h"

class OrderedVowelsParserImperative : public OrderedVowelsParser {
public: // OrderedVowelsParser
  void feed(int Ch) override;
};
