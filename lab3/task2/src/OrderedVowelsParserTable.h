#pragma once

#include "OrderedVowelsParser.h"

class OrderedVowelsParserTable : public OrderedVowelsParser {
public: // OrderedVowelsParser
  void feed(int Ch) override;
};
