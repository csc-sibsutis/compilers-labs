#include "OrderedVowelsParser.h"

#include <array>
#include <string>

OrderedVowelsParser::~OrderedVowelsParser() = default;

bool OrderedVowelsParser::isGood() const {
  return CurrentState != Error;
}
