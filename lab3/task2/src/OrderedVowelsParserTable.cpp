#include "OrderedVowelsParserTable.h"

#include <cctype>
#include <cstdio>

namespace {
enum CharClass { A, E, I, O, U, Cons, Eof, Other };
}

static CharClass getCharClass(int Ch) {
  if (Ch == 'a')
    return A;
  if (Ch == 'e')
    return E;
  if (Ch == 'i')
    return I;
  if (Ch == 'o')
    return O;
  if (Ch == 'u')
    return U;
  if (Ch == EOF)
    return Eof;

  if (isalpha(Ch) && islower(Ch))
    return Cons;

  return Other;
}

// Таблица переходов.
// Плюc: декларативненько
// Минусы: громоздко, сложно модифицировать
void OrderedVowelsParserTable::feed(int Ch) {
  const CharClass CurrentCharClass = getCharClass(Ch);
  // clang-format off
  State nextState[8][8] = {
                  //       a        e        i        o          u       Cons    EOF  Other
    /* ExpectA   */ {ExpectE,   Error,   Error,   Error,     Error,   ExpectA, Error, Error},
    /* ExpectE   */ {  Error, ExpectI,   Error,   Error,     Error,   ExpectE, Error, Error},
    /* ExpectI   */ {  Error,   Error, ExpectO,   Error,     Error,   ExpectI, Error, Error},
    /* ExpectO   */ {  Error,   Error,   Error, ExpectU,     Error,   ExpectO, Error, Error},
    /* ExpectU   */ {  Error,   Error,   Error,   Error, ExpectEOF,   ExpectU,   End, Error},
    /* ExpectEOF */ {  Error,   Error,   Error,   Error,     Error, ExpectEOF,   End, Error},
    /* End       */ {  Error,   Error,   Error,   Error,     Error,     Error, Error, Error},
    /* Error     */ {  Error,   Error,   Error,   Error,     Error,     Error, Error, Error}};
  // clang-format on
  CurrentState = nextState[CurrentState][CurrentCharClass];
}
