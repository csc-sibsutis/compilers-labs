#include "OrderedVowelsParserSwitch.h"

#include <cctype>
#include <cstdio>
#include <string>

static bool isValidChar(int Ch) {
  return (isalpha(Ch) && islower(Ch)) || Ch == EOF;
}

static bool isVowel(int Ch) {
  const std::string vowels = "aeiou";
  return vowels.find(tolower(Ch)) != std::string::npos;
}

// Тупой, механический способ, "по учебнику".
// Плюс: не нужно включать голову.
// Минус: громоздко, копипаста
void OrderedVowelsParserSwitch::feed(int Ch) {
  if (!isValidChar(Ch)) {
    CurrentState = Error;
    return;
  }

  switch (CurrentState) {
  case ExpectA:
    if (Ch == 'a') {
      CurrentState = ExpectE;
    } else if (isVowel(Ch) || Ch == EOF) {
      CurrentState = Error;
    }
    break;

  case ExpectE:
    if (Ch == 'e') {
      CurrentState = ExpectI;
    } else if (isVowel(Ch) || Ch == EOF) {
      CurrentState = Error;
    }
    break;

  case ExpectI:
    if (Ch == 'i') {
      CurrentState = ExpectO;
    } else if (isVowel(Ch) || Ch == EOF) {
      CurrentState = Error;
    }
    break;

  case ExpectO:
    if (Ch == 'o') {
      CurrentState = ExpectU;
    } else if (isVowel(Ch) || Ch == EOF) {
      CurrentState = Error;
    }
    break;

  case ExpectU:
    if (Ch == 'u') {
      CurrentState = ExpectEOF;
    } else if (isVowel(Ch) || Ch == EOF) {
      CurrentState = Error;
    }
    break;

  case ExpectEOF:
    if (Ch == EOF) {
      CurrentState = End;
    } else if (isVowel(Ch) || !isValidChar(Ch)) {
      CurrentState = Error;
    }
    break;

  case End:
    CurrentState = Error;
    break;

  case Error:
    // Nothing
    break;
  }
}
