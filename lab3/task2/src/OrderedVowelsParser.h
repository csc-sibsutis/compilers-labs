#pragma once

class OrderedVowelsParser {
public:
  virtual ~OrderedVowelsParser();

  virtual void feed(int Ch) = 0;

  bool isGood() const;

protected:
  enum State {
    ExpectA,
    ExpectE,
    ExpectI,
    ExpectO,
    ExpectU,
    ExpectEOF,
    End,
    Error
  };

  State CurrentState = ExpectA;
};
