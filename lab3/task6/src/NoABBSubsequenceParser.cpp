﻿#include "NoABBSubsequenceParser.h"

#include <cstdio>

namespace {
enum CharClass { A, B, Eof, Other };
} // namespace

static CharClass getCharClass(int Ch) {
  if (Ch == 'a')
    return A;
  if (Ch == 'b')
    return B;
  if (Ch == EOF)
    return Eof;
  return Other;
}

void NoABBSubsequenceParser::feedChar(int Ch) {
  // clang-format off
  State nextState[7][4] = {
    //                   A         B    EOF  Other
    /* Empty    */ {  GotA, NonEmpty, Error, Error},
    /* NonEmpty */ {  GotA, NonEmpty,   End, Error},
    /* GotA     */ {  GotA,    GotAB,   End, Error},
    /* GotAB    */ { GotAB,   GotABB,   End, Error},
    /* GotABB   */ {GotABB,   GotABB, Error, Error},
    /* End      */ { Error,    Error, Error, Error},
    /* Error    */ { Error,    Error, Error, Error}
  };
  // clang-format on

  const CharClass CurrentCharClass = getCharClass(Ch);
  CurrentState = nextState[CurrentState][CurrentCharClass];
}

bool NoABBSubsequenceParser::isGood() const {
  return CurrentState != Error;
}
