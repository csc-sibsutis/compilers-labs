﻿#pragma once

class NoABBSubsequenceParser {
public:
  void feedChar(int Ch);
  bool isGood() const;

private:
  enum State { Empty, NonEmpty, GotA, GotAB, GotABB, End, Error };

  State CurrentState = Empty;
};
