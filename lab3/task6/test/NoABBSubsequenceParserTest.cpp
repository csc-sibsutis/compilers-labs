﻿#include <NoABBSubsequenceParser.h>

#include <string>

#include <gtest/gtest.h>

class NoABBSubsequenceParserTest : public ::testing::Test {
protected:
  void feedString(NoABBSubsequenceParser &Parser, const std::string &S) const {
    for (const auto Ch : S) {
      Parser.feedChar(Ch);
    }
    Parser.feedChar(EOF);
  }

  bool isValidString(const std::string &S) const {
    NoABBSubsequenceParser Parser;
    feedString(Parser, S);
    return Parser.isGood();
  }
};

TEST_F(NoABBSubsequenceParserTest, EmptyString) {
  EXPECT_FALSE(isValidString(""));
}

TEST_F(NoABBSubsequenceParserTest, ValidStrings) {
  EXPECT_TRUE(isValidString("a"));
  EXPECT_TRUE(isValidString("ab"));
  EXPECT_TRUE(isValidString("aab"));
  EXPECT_TRUE(isValidString("b"));
  EXPECT_TRUE(isValidString("bb"));
  EXPECT_TRUE(isValidString("bba"));
}

TEST_F(NoABBSubsequenceParserTest, InvalidStrings) {
  // Differs from abb substring
  EXPECT_FALSE(isValidString("abab"));

  EXPECT_FALSE(isValidString("abb"));
  EXPECT_FALSE(isValidString("aabb"));
  EXPECT_FALSE(isValidString("abbb"));
  EXPECT_FALSE(isValidString("aabbb"));
  EXPECT_FALSE(isValidString("az"));
  EXPECT_FALSE(isValidString("AB"));
}
