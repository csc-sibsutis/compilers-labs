﻿#include "AlphabetParser.h"

#include <cctype>

void AlphabetParser::feedChar(int Ch) {
  if (!islower(Ch) || !isalpha(Ch)) {
    CurrentState = Error;
    return;
  }

  if (CurrentState == Error) {
    return;
  }

  if (LastChar <= Ch) {
    LastChar = Ch;
  } else {
    CurrentState = Error;
  }
}

bool AlphabetParser::isGood() const {
  return CurrentState == Good;
}
