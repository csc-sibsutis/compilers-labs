﻿#pragma once

class AlphabetParser {
public:
  void feedChar(int Ch);
  bool isGood() const;

private:
  enum State { Good, Error };

  State CurrentState = Good;
  int LastChar = 'a';
};
