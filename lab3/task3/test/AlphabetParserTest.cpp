﻿#include <AlphabetParser.h>

#include <gtest/gtest.h>

class AlphabetParserTest : public ::testing::Test {
protected:
  void feedString(AlphabetParser &Parser, const std::string &S) const {
    for (const auto Ch : S) {
      Parser.feedChar(Ch);
    }
  }

  bool isSequenced(const std::string &S) const {
    AlphabetParser Parser;
    feedString(Parser, S);
    return Parser.isGood();
  }
};

TEST_F(AlphabetParserTest, EmptyString) {
  EXPECT_TRUE(isSequenced(""));
}

TEST_F(AlphabetParserTest, HappyCases) {
  EXPECT_TRUE(isSequenced("abcd"));
  EXPECT_TRUE(isSequenced("az"));
  EXPECT_TRUE(isSequenced("by"));
  EXPECT_TRUE(isSequenced("aaabbbccczzz"));
}

TEST_F(AlphabetParserTest, Unsequenced) {
  EXPECT_FALSE(isSequenced("aba"));
  EXPECT_FALSE(isSequenced("za"));
}
