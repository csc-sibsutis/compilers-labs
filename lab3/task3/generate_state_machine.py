import string
from itertools import zip_longest


def main():
    print('digraph G {')
    pairs = zip_longest(string.ascii_lowercase, string.ascii_lowercase[1:])

    for first, second in pairs:
        print(f'  {first} -> {first} [label={first}]')
        if second is not None:
            print(f'  {first} -> {second} [label={second}]')

    print('}')


if __name__ == '__main__':
    main()